package com.kraftwerking.rampup;

import java.sql.Connection;
import java.sql.DriverManager;
//import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.kraftwerking.rampup.dbutil.JDBCConnection;

public class BasicJDBC {
	  private Connection connect = null;
	  private Statement statement = null;
	  //private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;

	  public void readDataBase() throws Exception {
		JDBCConnection jdbcconn = new JDBCConnection();
	    try {
	      // this will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // setup the connection with the DB.
	      connect = jdbcconn.getConnection();

	      // statements allow to issue SQL queries to the database
	      statement = connect.createStatement();
	      // resultSet gets the result of the SQL query
	      resultSet = statement
	          .executeQuery("select * from PERSON,VEHICLE");
	      writeMetaData(resultSet);
	      writeResultSet(resultSet);
	      
	    } catch (Exception e) {
	      throw e;
	    } finally {
	    	statement.close();
	    	resultSet.close();
	    	connect.close();
	    }

	  }

	  private void writeMetaData(ResultSet resultSet) throws SQLException {
	    // now get some metadata from the database
	    System.out.println("The columns in the table are: ");
	    System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
	    for  (int i = 1; i<= resultSet.getMetaData().getColumnCount(); i++){
	      System.out.println("Column " +i  + " "+ resultSet.getMetaData().getColumnName(i));
	    }
	  }

	  private void writeResultSet(ResultSet resultSet) throws SQLException {
	    // resultSet is initialised before the first data set
	    while (resultSet.next()) {
	      // it is possible to get the columns via name
	      // also possible to get the columns via the column number
	      // which starts at 1
	      // e.g., resultSet.getSTring(2);
	      String person_id = resultSet.getString("person_id");
	      String person_name = resultSet.getString("person_name");
	      String vehicle_id = resultSet.getString("vehicle_id");
	      String vehicle_name = resultSet.getString("vehicle_name");

	      System.out.println("Person ID: " + person_id);
	      System.out.println("Person Name: " + person_name);
	      System.out.println("Vehicle ID: " + vehicle_id);
	      System.out.println("Vehicle Name: " + vehicle_name);
	    }
	  }
}
