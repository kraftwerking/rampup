package com.kraftwerking.rampup.model;

public class Vehicle {
	int vehicleId;
	Person person;
	String vehicleName;
	
	public Vehicle(int vehicleId, Person person, String vehicleName) {
		super();
		this.vehicleId = vehicleId;
		this.person = person;
		this.vehicleName = vehicleName;
	}
	public int getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public String getVehicleName() {
		return vehicleName;
	}
	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}
	
	

}
