package com.kraftwerking.rampup.dao;

import java.util.ArrayList;

import com.kraftwerking.rampup.model.Person;
import com.kraftwerking.rampup.model.Vehicle;

public class BasicDaoClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BasicDao dao = new BasicDao();
		for(int x = 0; x < 10; x++) {
			try {
				Person person = dao.getPersonById(x);
				if(person != null){
					System.out.println("-------");
					System.out.println("Person ID: " + person.getPersonId());
					System.out.println("Person Name: " + person.getPersonName());
					System.out.println("Vehicles owned by " + person.getPersonName()
							+ ":");
					ArrayList<Vehicle> vehicles = dao.getVehiclesOwnedByPerson(person);
					for (Vehicle vehicle : vehicles) {
						System.out.println("Vehicle ID: " + vehicle.getVehicleId());
						System.out.println("Vehicle Name: " + vehicle.getVehicleName());
					}	
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
