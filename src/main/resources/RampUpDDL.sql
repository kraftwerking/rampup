CREATE TABLE `person` (
  `person_id` int(11) NOT NULL,
  `person_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `vehicle` (
  `vehicle_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `vehicle_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`vehicle_id`),
  CONSTRAINT `vehicle_ibfk_1` FOREIGN KEY (`vehicle_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;