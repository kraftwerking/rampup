package com.kraftwerking.rampup.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.kraftwerking.rampup.dbutil.JDBCConnection;
import com.kraftwerking.rampup.model.Person;
import com.kraftwerking.rampup.model.Vehicle;

public class BasicDao {
	  private Connection connect = null;
	  private Statement statement = null;
	  private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;

	  public Person getPersonById(int person_id) throws Exception {
		Person person = null;
		JDBCConnection jdbcconn = new JDBCConnection();
	    try {
	      // this will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // setup the connection with the DB.
	      connect = jdbcconn.getConnection();

	   // preparedStatements can use variables and are more efficient
	      preparedStatement = connect
	          .prepareStatement("SELECT * FROM person WHERE person_id = ?;");
	      preparedStatement.setInt(1, person_id);
	      resultSet = preparedStatement.executeQuery();

	      writeMetaData(resultSet);
	      person = getPerson(resultSet);
	      return person;
	      
	    } catch (Exception e) {
	      throw e;
	    } finally {
	    	preparedStatement.close();
	    	resultSet.close();
	    	connect.close();
	    }

	  }
	  
	  public ArrayList<Vehicle> getVehiclesOwnedByPerson(Person person) throws Exception {
		ArrayList<Vehicle> vehicles = null;
		Vehicle vehicle = null;
		JDBCConnection jdbcconn = new JDBCConnection();
	    try {
	      // this will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // setup the connection with the DB.
	      connect = jdbcconn.getConnection();

	   // preparedStatements can use variables and are more efficient
	      preparedStatement = connect
	          .prepareStatement("SELECT * FROM vehicle WHERE person_id = ?;");
	      preparedStatement.setInt(1, person.getPersonId());
	      resultSet = preparedStatement.executeQuery();

	      writeMetaData(resultSet);
	      vehicles = getVehicles(resultSet, person);

	      return vehicles;
	      
	    } catch (Exception e) {
	      throw e;
	    } finally {
	    	preparedStatement.close();
	    	resultSet.close();
	    	connect.close();
	    }

	  }

	  private void writeMetaData(ResultSet resultSet) throws SQLException {
	    // now get some metadata from the database
	    //System.out.println("The columns in the table are: ");
	    //System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
	    for  (int i = 1; i<= resultSet.getMetaData().getColumnCount(); i++){
	      //System.out.println("Column " +i  + " "+ resultSet.getMetaData().getColumnName(i));
	    }
	  }

	  private Person getPerson(ResultSet resultSet) throws SQLException {
		Person person = null;
	    // resultSet is initialised before the first data set
	    while (resultSet.next()) {
	      // it is possible to get the columns via name
	      // also possible to get the columns via the column number
	      // which starts at 1
	      // e.g., resultSet.getSTring(2);
	      String person_id = resultSet.getString("person_id");
	      String person_name = resultSet.getString("person_name");

	      //System.out.println("Person ID: " + person_id);
	      //System.out.println("Person Name: " + person_name);
	      
	      person = new Person(Integer.valueOf(person_id), person_name);
	    }
		return person;
	  }
	  
	  private ArrayList<Vehicle> getVehicles(ResultSet resultSet, Person person) throws SQLException {
			ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
		  Vehicle vehicle = null;
	    // resultSet is initialised before the first data set
	    while (resultSet.next()) {
	      // it is possible to get the columns via name
	      // also possible to get the columns via the column number
	      // which starts at 1
	      // e.g., resultSet.getSTring(2);
	      String vehicle_id = resultSet.getString("vehicle_id");
	      String vehicle_name = resultSet.getString("vehicle_name");

	      //System.out.println("Vehicle ID: " + vehicle_id);
	      //System.out.println("Person ID: " + person.getPersonId());
	      //System.out.println("Vehicle Name: " + vehicle_name);
	      
	      vehicle = new Vehicle(Integer.valueOf(vehicle_id), person, vehicle_name);
	      vehicles.add(vehicle);
	    }
		return vehicles;
	  }
}
