package com.kraftwerking.rampup.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;

public class JDBCConnection {
	
	public Connection getConnection() throws Exception{
		Connection connect = null;
		
		try {
		      // this will load the MySQL driver, each DB has its own driver
		      Class.forName("com.mysql.jdbc.Driver");
		      // setup the connection with the DB.
		      connect = DriverManager
		          .getConnection("jdbc:mysql://localhost/rampup?"
		              + "user=root"); //fix

		    } catch (Exception e) {
		      throw e;
		    }
		
		return connect;
	}
}
